#ifndef LEIDEN_LEIDENALGORITHM_HPP
#define LEIDEN_LEIDENALGORITHM_HPP

#include <networkit/community/CommunityDetectionAlgorithm.hpp>
#include "networkit/structures/Partition.hpp"
#include "networkit/graph/Graph.hpp"
#include "networkit/Globals.hpp"
#include "CommunitySubGraph.hpp"

using namespace NetworKit;

namespace NetworKit {

    class LeidenAlgorithm final : public CommunityDetectionAlgorithm {
    public:
        explicit LeidenAlgorithm(const Graph &graph);

        LeidenAlgorithm(const Graph &graph, Partition &initialPartition);

        void run() override;

        Partition getPartition() override;

        std::vector<CommunitySubGraph> getCommunitySubGraphs();

        const Graph &getGraph();

    private:
        const Graph *G;

        Partition zeta;
    };

}

#endif //LEIDEN_LEIDENALGORITHM_HPP
