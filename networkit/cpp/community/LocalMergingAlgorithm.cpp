#include <iostream>
#include <random>
#include "networkit/community/LocalMergingAlgorithm.hpp"
#include "networkit/Globals.hpp"
#include "networkit/graph/Graph.hpp"
#include "networkit/structures/Partition.hpp"
#include <networkit/auxiliary/SignalHandling.hpp>

namespace NetworKit {

    LocalMergingAlgorithm::LocalMergingAlgorithm(const Graph &graph, double gamma, double randomness) : graph(graph), gamma(gamma), randomness(randomness) {}

    Partition LocalMergingAlgorithm::findSubGraphPartition(CommunitySubGraph subGraph) {
        count z = subGraph.graph.upperNodeIdBound();

        // Start with a singleton partition
        Partition zetaSubGraph(z);
        zetaSubGraph.allToSingletons();
        index o = zetaSubGraph.upperBound();

        // We only want to move nodes that are in singleton clusters
        std::vector<bool> isClusterNonSingleton(o, false);

        edgeweight totalEdgeWeight = subGraph.graph.totalEdgeWeight();

        edgeweight fullGraphTotalEdgeWeight = graph.totalEdgeWeight();

        auto divisor = 2 * fullGraphTotalEdgeWeight * fullGraphTotalEdgeWeight;

        // Accumulate volumes for all nodes in the subgraph
        std::vector<double> nodeVolumes(z, 0.0);
        subGraph.graph.parallelForNodes([&](node u) {
            nodeVolumes[u] += graph.weightedDegree(u, true);
        });

        // We always start with a singleton partition so nodeVolumes = communityVolumes
        std::vector<double> communityVolumes(nodeVolumes);

        if (subGraph.graph.numberOfNodes() == 1) {
            return zetaSubGraph;
        }

        // The number of external edges (no self loops) per node
        std::vector<edgeweight> affinity(o);
        std::vector<index> neighbors(o);

        // The quality increase achieved by moving a node to a respective cluster
        std::vector<double> qualityIncrementPerCluster(o);

        bool update = false;

        auto modularityGain = [&](node u, index C, index D, edgeweight affinityC, edgeweight affinityD) {
            double nodeVolume = nodeVolumes[u];
            auto dMinusU = communityVolumes[D];
            double delta = affinityD / fullGraphTotalEdgeWeight -
                           gamma * (dMinusU * nodeVolume) / divisor;
            return delta;
        };

        std::vector<edgeweight> cuts(z, 0.0);
        subGraph.graph.forNodes([&](node u) {
            subGraph.graph.forNeighborsOf(u, [&](node v, edgeweight ew) {
                if (u != v) {
                    cuts[u] += ew;
                }
            });
        });

        // Visit all nodes in the subgraph in random order...
        subGraph.graph.forNodesInRandomOrder([&](node u) {
            index C = zetaSubGraph[u];

            // Affinity
            neighbors.clear();
            subGraph.graph.forNeighborsOf(u, [&](node v) {
                index D = zetaSubGraph[v];
                affinity[D] = -1;
            });
            affinity[C] = 0.0;
            // We want to be able to move the node back to its original cluster
            neighbors.push_back(C);
            subGraph.graph.forNeighborsOf(u, [&](node v, edgeweight ew) {
                if (u != v) {
                    index D = zetaSubGraph[v];
                    if (affinity[D] == -1) {
                        affinity[D] = 0;
                        neighbors.push_back(D);
                    }
                    affinity[D] += ew;
                }
            });

            // ...but only consider nodes in singleton communities as they have not been moved yet
            if (!isClusterNonSingleton[C] &&
                (cuts[u] >= nodeVolumes[u] * (totalEdgeWeight - nodeVolumes[u]) / fullGraphTotalEdgeWeight)) {
                // Remove node from its cluster
                communityVolumes[C] = 0;
                affinity[C] = 0;

                index bestCluster = C;
                double maxQualityIncrement = 0;
                double totalQualityIncrement = 0;

                index neighborIndex = 0;
                // Find the neighbor community that yields the highest modularity gain
                for (auto D: neighbors) {
                    if (cuts[D] >=
                        communityVolumes[D] * (totalEdgeWeight - communityVolumes[D]) / fullGraphTotalEdgeWeight) {
                        auto delta = modularityGain(u, C, D, affinity[C], affinity[D]);

                        if (delta > maxQualityIncrement) {
                            bestCluster = D;
                            maxQualityIncrement = delta;
                        }

                        if (delta >= 0) {
                            totalQualityIncrement += std::exp((delta * totalEdgeWeight) / randomness);
                        }
                    }

                    // Keep track of the quality gains in a prefix sum kind of way
                    qualityIncrementPerCluster[neighborIndex] = totalQualityIncrement;
                    neighborIndex += 1;
                }

                index chosenCluster = bestCluster;

                if (totalQualityIncrement < std::numeric_limits<double>::max()) {
                    // Start at a random "position" between 0 and the sum of quality increments
                    auto r = totalQualityIncrement * Aux::Random::real();

                    int64_t min_idx = -1;
                    int64_t max_idx = neighbors.size() + 1;

                    // Linear search to find the cluster with a quality increment larger than r
                    while (min_idx < max_idx - 1) {
                        auto mid_idx = (min_idx + max_idx) / 2;
                        if (qualityIncrementPerCluster[mid_idx] >= r) {
                            max_idx = mid_idx;
                        } else {
                            min_idx = mid_idx;
                        }
                    }

                    chosenCluster = neighbors[max_idx];
                }

                // Move node to its new community
                communityVolumes[chosenCluster] += nodeVolumes[u];

                for (auto D: neighbors) {
                    affinity[D] = 0;
                }

                if (chosenCluster != C) {
                    zetaSubGraph[u] = chosenCluster;
                    cuts[chosenCluster] += cuts[C] - 2 * affinity[chosenCluster];
                    isClusterNonSingleton[chosenCluster] = true;
                    update = true;
                }
            }
        });

        if (update) {
            zetaSubGraph.compact();
        }

        zetaSubGraph.forEntries([&](node u, index C) {
            assert(C < z);
            assert(C != none);
        });

        return zetaSubGraph;
    }

}